terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}
variable "runner_token" {
  description = "runner token"
}

variable "gitlab_url" {
  description = "runner token"
  default = "https://gitlab.com/"
}

variable "runner_tags" {
  description = "runner tags"
  default = "docker"
}
variable "runner_name" {
  description = "docker runner name"
  default = "my_local_gitlab_runner"
}


variable "gitlab_runner_image" {
  description = "gitlab runner image"
  default = "gitlab/gitlab-runner:ubuntu"
}
variable "runner_base_image" {
  description = ""
  default = "alpine"
}
variable "docker_connection_type" {
  default = "NON_secure"
  description = "accepted values NON_secure or secure"
}
variable "runner_data_path" {
  description = "runner_data_path"
  default = "/tmp"
}
variable "docker_sock_path" {
  description = "the docker sock path "
  default = "/var/run/docker.sock"
}
variable "docker_host" {
  description = "the docker host address"
  default = ""
}
variable "docker_cert_path" {
  description = "the docker certificate folder path "
  default = ""
}



data "docker_registry_image" "gitlab_runner" {
  name = var.gitlab_runner_image
}
data "docker_registry_image" "runner_base_image" {
  name = var.runner_base_image
}


### RUNNER
resource "docker_image" "runner" {
  name = data.docker_registry_image.gitlab_runner.name
  pull_triggers = [data.docker_registry_image.gitlab_runner.sha256_digest]
  keep_locally = true
}


resource "docker_container" "runner" {
  image = docker_image.runner.latest
  name = var.runner_name
  restart = "always"
  volumes {
    host_path = var.docker_sock_path
    container_path = "/var/run/docker.sock"
    read_only = false
  }
  volumes {
    host_path = var.runner_data_path
    container_path = "/etc/gitlab-runner"
  }

  network_mode = "host"

}

resource "null_resource" "register_runner" {
  depends_on = [
    docker_container.runner
  ]

  provisioner "local-exec" {

    command = var.docker_connection_type == "NON_secure" ?"sleep 5 && docker run --rm -v ${var.runner_data_path}:/etc/gitlab-runner gitlab/gitlab-runner register      --non-interactive     --executor \"docker\"   --docker-image ${data.docker_registry_image.runner_base_image.name}      --url ${var.gitlab_url}      --registration-token ${var.runner_token}    --description ${var.runner_name}   --tag-list ${var.runner_tags}    --run-untagged=\"true\"     --locked=\"false\"   --access-level=\"not_protected\" --docker-volumes /var/run/docker.sock:/var/run/docker.sock": " export DOCKER_HOST=${var.docker_host}  && export DOCKER_TLS_VERIFY=1 && export DOCKER_CERT_PATH=${var.docker_cert_path} && sleep 5 && docker run --rm -v ${var.runner_data_path}:/etc/gitlab-runner gitlab/gitlab-runner register      --non-interactive     --executor \"docker\"   --docker-image ${data.docker_registry_image.runner_base_image.name}      --url ${var.gitlab_url}      --registration-token ${var.runner_token}    --description ${var.runner_name}   --tag-list ${var.runner_tags}    --run-untagged=\"true\"     --locked=\"false\"   --access-level=\"not_protected\" --docker-volumes /var/run/docker.sock:/var/run/docker.sock"
  }
}
resource "null_resource" "restart_runner" {
  depends_on = [
    docker_container.runner,null_resource.register_runner
  ]
  triggers = {
    always = uuid()
  }
  provisioner "local-exec" {
    command = var.docker_connection_type == "NON_secure" ? "docker restart ${docker_container.runner.name}":"export DOCKER_HOST=${var.docker_host} && export DOCKER_TLS_VERIFY=1 && export DOCKER_CERT_PATH=${var.docker_cert_path} && docker restart ${docker_container.runner.name}"
  }
}
